#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "search.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    search* g;
    bool ageall,salall,weghtall,heghtall;
private slots:
    void openFileSlot();
    void sslot();
    void ageSlot();
    void salSlot();
    void weghtSlot();
    void heghtSlot();
};

#endif // MAINWINDOW_H
