#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include "QMessageBox"
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ageall=salall=weghtall=heghtall=false;
    ui->setupUi(this);
   // this->connect(this->ui->pushButton,SIGNAL(clicked()),SLOT(openFileSlot()));
    this->connect(this->ui->agech,SIGNAL(clicked()),SLOT(ageSlot()));
    this->connect(this->ui->salch,SIGNAL(clicked()),SLOT(salSlot()));
    this->connect(this->ui->wch,SIGNAL(clicked()),SLOT(weghtSlot()));
    this->connect(this->ui->hch,SIGNAL(clicked()),SLOT(heghtSlot()));
}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::openFileSlot(){
    QFileDialog* fd = new QFileDialog();
    //fd->show();
    QFile f(fd->getOpenFileName(NULL,tr("Open File"),"j:\\","*.txt"));
    this->g= new search(f);
}

void MainWindow::sslot(){

    QSet<int> result=this->g->setParam(ui->spinBox->value(),ui->spinBox_2->value(),ui->doubleSpinBox->value(),
                                       ui->doubleSpinBox_2->value(),ui->spinBox_3->value(),ui->spinBox_4->value(),
                                       ui->spinBox_5->value(),ui->spinBox_6->value(),ageall,salall,weghtall,heghtall);
   ui->listWidget->clear();
    foreach(int item,result)
        this->ui->listWidget->addItem("Запись № "+QString::number(item));
    return ;
}

void MainWindow::ageSlot()
{
 ageall=!ageall;
 ui->spinBox->setEnabled(!ageall);
 ui->spinBox_2->setEnabled(!ageall);
 return;
}

void MainWindow::salSlot()
{
 salall=!salall;
 ui->doubleSpinBox->setEnabled(!salall);
 ui->doubleSpinBox_2->setEnabled(!salall);
 return;
}

void MainWindow::weghtSlot()
{
 weghtall=!weghtall;
 ui->spinBox_5->setEnabled(!weghtall);
 ui->spinBox_6->setEnabled(!weghtall);
 return;

}

void MainWindow::heghtSlot()
{
    heghtall=!heghtall;
    ui->spinBox_3->setEnabled(!heghtall);
    ui->spinBox_4->setEnabled(!heghtall);
    return;
}
