#ifndef SEARCH_H
#define SEARCH_H
#include "QtCore"
#include "QtGui"
#include <QApplication>
struct d{
    int id;
    int age;
    double sal;
    int w;
    int h;
    QString toStr();
};

class search
{
private:
    QPair<int,int> _age;
    QPair<double,double> _salary;
    QPair<int,int> _h;
    QPair<int,int> _w;
    QVector<QList<int> > _ageItems;
    QVector<QList<int> > _wItems;
    QVector<QList<int> > _hItems;
    QHash<QString,QList<int> > _salIteml;
    QVector<d> alll;
    void parce(QString);

    void addHeight(int ps, int pe, QSet<int> &r);
    void addAge(int as, int ae, QSet<int>& r);
    void filterHeight(int ps, int pe, QSet<int>& r);
    void filterWeight(int ws, QSet<int>& r, int we);
    void filterSalary(double ss, QSet<int>& r, double se);
    void addWeigtht(int ws, int we, QSet<int> &r);
    void addSal(double ss, QSet<int>& r, double se);
public:
    search(QFile& file);
    QSet<int> setParam(int as, int ae, double ss,double se,int ps,int pe, int ws, int we,bool e,bool s, bool w,bool h);

};

#endif // SEARCH_H
