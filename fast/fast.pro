#-------------------------------------------------
#
# Project created by QtCreator 2013-10-30T23:24:29
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = fast
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    search.cpp

HEADERS  += mainwindow.h \
    search.h

FORMS    += mainwindow.ui
